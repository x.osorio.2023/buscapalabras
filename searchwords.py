#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''
import sys
import sortwords

def search_word(word, words_list):
    ...
    word = word.lower()
    for words in range(len(words_list)):
        words_list[words] = words_list[words].lower()
    test_index = -1
    try:
        for words in range(len(words_list)):
            if words_list[words] == word:
                test_index = words
        if test_index == -1:
            raise (Exception)

        return test_index
    except:

         raise Exception


def main():
    ...
    try:
        word = sys.argv[1]
        list = sys.argv[2:]
    except IndexError:
        sys.exit("La lista ha de tener mas valores")
    ordered_list = sortwords.sort(list)
    sortwords.show(ordered_list)
    try:
        position = search_word(word, ordered_list)
        print(position)

    except:
        sys.exit("La palabra introducida no es correcta")

if __name__ == '__main__':
            main()
